use crate::http_method::HttpMethod;
use crate::http_request::HttpRequest;
use crate::http_response::HttpResponse;
use crate::http_transfer_encoding::HttpTransferEncoding;
use crate::parsers::{parse_request, parse_response};
use crate::ssl_manager::get_host_pair;
use crate::config::Config;

use chrono::prelude::*;
use core::pin::Pin;
use openssl::{
    pkey::{PKey, Private},
    ssl::{HandshakeError, Ssl, SslAcceptor, SslConnector, SslMethod, SslVerifyMode},
    x509::X509,
};
use rusqlite::Connection;
use std::{
    io::{Read, Write},
    net::TcpStream as LTcpStream,
    time::{Duration, Instant},
};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
};
use tokio_openssl::SslStream;
use log::{info, error};


pub async fn client_manager(mut s: TcpStream, ca_cert: X509, ca_key_pair: PKey<Private>, config: Config) {
    let req = handle_request_a(&mut s, false).await;
    let req = match req {
        None => return,
        Some(x) => x,
    };

    match req.method() {
        HttpMethod::Connect => {
            /* https */
            handle_https_request(s, req, ca_cert, ca_key_pair, config).await;
        }
        _ => {
            /* http */
            handle_cleartext_request(s, req, &ca_cert, config).await;
        }
    }
}

async fn handle_cleartext_request(mut s: TcpStream, req: HttpRequest, ca_cert: &X509, config: Config) {
    let destination = req.destination().unwrap();

    if destination == &config.full_addr() && req.path() == "/cacert" {
        let pem = ca_cert.to_pem().unwrap();
        s.write_all(
            format!("HTTP/1.0 200 OK\r\nContent-Type: application/x-x509-ca-cert\r\nContent-Length: {}\r\nContent-Disposition: inline; filename=\"cacert.der\"\r\n\r\n", pem.len()).as_bytes()
        ).await.unwrap();
        s.write_all(&pem).await.unwrap();
    } else {
        let now = Instant::now();
        let mut rstream = LTcpStream::connect(destination).unwrap();
        rstream.write_all(&req.as_bytes()).unwrap();

        let resp = handle_response(&mut rstream);
        s.write_all(&resp.as_bytes()).await.unwrap();
        info!(
            "{} {} [{}] - {:?}",
            req.method(),
            req.path(),
            resp.status(),
            now.elapsed()
        );
        insert_complete_request(&req, &resp, false, &config, &now.elapsed()).await;
    }
}

async fn handle_https_request(
    mut s: TcpStream,
    req: HttpRequest,
    ca_cert: X509,
    ca_key_pair: PKey<Private>,
    config: Config,
) {
    let now = Instant::now();
    let conn = Connection::open(format!("{}/hist.db", config.directory())).unwrap();
    let destination = req.destination().unwrap();
    let mut host_no_port = req.destination_base().unwrap().to_owned();

    if host_no_port.contains(':') {
        host_no_port = host_no_port.split(':').take(1).collect();
    }

    s.write_all("HTTP/1.0 200 OK\r\nProxy-Connection: Close\r\n\r\n".as_bytes())
        .await
        .unwrap();

    let (cert, pair) = get_host_pair(&conn, &host_no_port, &ca_cert, &ca_key_pair);
    let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    acceptor.set_private_key(&pair).unwrap();
    acceptor.set_certificate(&cert).unwrap();
    acceptor.add_extra_chain_cert(ca_cert.clone()).unwrap();
    acceptor.set_verify(SslVerifyMode::NONE);
    acceptor.check_private_key().unwrap();
    let acceptor = acceptor.build();

    let tok_ssl = Ssl::new(acceptor.context()).unwrap();
    let mut tok_sslstream = SslStream::new(tok_ssl, s).unwrap();

    let mut ok = false;
    for _ in 0..5 {
        match Pin::new(&mut tok_sslstream).accept().await {
            Ok(_) => {
                ok = true;
                break;
            }
            Err(_) => error!(
                "Error during SSL Handshake. Maybe the client did not accept the certificate?",
            ),
        }
    }

    if !ok {
        return;
    }

    let req = match handle_request_a(&mut tok_sslstream, true).await {
        Some(r) => r,
        None => {
            error!("Error while reading https request");
            return;
        },
    };

    let rstream_std = match LTcpStream::connect(&destination) {
        Ok(r) => r,
        Err(_) => {
            error!("Could not connect to host {}", destination);
            return;
        },
    };

    rstream_std
        .set_read_timeout(Some(Duration::from_millis(3000)))
        .unwrap();

    let mut rstream = match ssl_connect(&rstream_std, &host_no_port) {
        Some(r) => r,
        None => {
            error!("Error handshaking with {}", destination);
            return;
        }
    };

    rstream.write_all(&req.as_bytes()).unwrap();
    let resp = handle_response(&mut rstream);
    tok_sslstream.write_all(&resp.as_bytes()).await.unwrap();
    info!(
        "{} https://{}{} [{}] - {:?}",
        req.method(),
        req.destination_base().unwrap_or(&"?".to_string()),
        req.path(),
        resp.status(),
        now.elapsed()
    );
    insert_complete_request(&req, &resp, true, &config, &now.elapsed()).await;
    let mut keep_alive = resp.keep_alive();
    while keep_alive {
        let req = handle_request_a(&mut tok_sslstream, true).await;
        let req = match req {
            None => return,
            Some(x) => x,
        };
        rstream.write_all(&req.as_bytes()).unwrap();
        let resp = handle_response(&mut rstream);
        tok_sslstream.write_all(&resp.as_bytes()).await.unwrap();
        info!(
            "{} https://{}{} [{}] - {:?}",
            req.method(),
            req.destination_base().unwrap_or(&"?".to_string()),
            req.path(),
            resp.status(),
            now.elapsed()
        );
        insert_complete_request(&req, &resp, true, &config, &now.elapsed()).await;
        keep_alive = resp.keep_alive();
    }
}

fn handle_response<T>(rstream: &mut T) -> HttpResponse
where
    T: Read + Write,
{
    let mut response_vu8: Vec<u8> = vec![];
    let (_body, mut resp) = loop {
        let mut buff_au8: [u8; 16 * 1024] = [0; 16 * 1024];
        if let Ok(n) = rstream.read(&mut buff_au8) {
            response_vu8.extend_from_slice(&buff_au8[..n]);
        }

        let (body, resp) = match parse_response(&response_vu8) {
            Ok((body, resp)) => (body, resp),
            Err(_err) => continue,
        };

        if resp.is_header_terminated() {
            break (body, resp);
        }
    };

    match resp.transfer_encoding() {
        HttpTransferEncoding::Chunked => {
            while !resp.is_transfer_complete() {
                let body = resp.body_mut();
                let mut buff_au8: [u8; 16 * 1024] = [0; 16 * 1024];
                if let Ok(n) = rstream.read(&mut buff_au8) {
                    body.extend_from_slice(&buff_au8[..n]);
                }
            }
        }
        _ => {
            while !resp.is_terminated() {
                let body = resp.body_mut();
                let mut buff_au8: [u8; 16 * 1024] = [0; 16 * 1024];
                if let Ok(n) = rstream.read(&mut buff_au8) {
                    body.extend_from_slice(&buff_au8[..n]);
                }
            }
        }
    }

    resp
}

async fn handle_request_a<T>(rstream: &mut T, ssl: bool) -> Option<HttpRequest>
where
    T: AsyncReadExt + AsyncWriteExt + std::marker::Unpin,
{
    let mut err_count = 0;
    let mut request_vu8: Vec<u8> = vec![];
    let (_body, mut req) = loop {
        let mut buff_au8: [u8; 16 * 1024] = [0; 16 * 1024];
        if let Ok(n) = rstream.read(&mut buff_au8).await {
            request_vu8.extend_from_slice(&buff_au8[..n]);
        }

        let (body, req) = match parse_request(&request_vu8, ssl) {
            Ok((body, req)) => (body, req),
            Err(_err) => {
                err_count += 1;
                if err_count > 10 {
                    return None;
                }
                continue;
            }
        };

        if req.is_header_terminated() {
            break (body, req);
        }
    };
    while !req.is_terminated() {
        let body = req.body_mut();
        let mut buff_au8: [u8; 16 * 1024] = [0; 16 * 1024];
        if let Ok(n) = rstream.read(&mut buff_au8).await {
            body.extend_from_slice(&buff_au8[..n]);
        }
    }

    Some(req)
}

async fn insert_complete_request(req: &HttpRequest, resp: &HttpResponse, ssl: bool, config: &Config, response_time: &std::time::Duration) {
    let conn = Connection::open(format!("{}/hist.db", config.directory())).expect("Could not open DB");

    let uri = req.path();
    let method = format!("{}", req.method());
    let size = format!("{}", req.as_bytes().len());
    let raw = String::from_utf8_lossy(&req.as_bytes()).to_string();
    let ssl = match ssl {
        true => "1".to_string(),
        false => "0".to_string(),
    };
    let raw_resp = String::from_utf8_lossy(&resp.as_bytes()).to_string();
    let status = format!("{}", resp.status().code());
    let params = {
        match req.method() {
            HttpMethod::Post | HttpMethod::Put | HttpMethod::Patch => "1".to_string(),
            _ => {
                if req.path().contains('?') {
                    "1".to_string()
                } else {
                    "0".to_string()
                }
            }
        }
    };
    let raw_resp_time = format!("{:?}", response_time);
    conn.execute(
        "INSERT INTO history (uri, method, size, params, status, raw, ssl, response, response_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)", 
        [uri, &method, &size, &params, &status, &raw, &ssl, &raw_resp, &raw_resp_time]
    ).unwrap();
}

fn ssl_connect<'a>(
    rstream_std: &'a LTcpStream,
    host_no_port: &str,
) -> Option<openssl::ssl::SslStream<&'a LTcpStream>> {
    let mut err_count = 0;
    let mut connectorb = SslConnector::builder(SslMethod::tls()).unwrap();
    connectorb.set_verify(SslVerifyMode::NONE);
    let connector = connectorb.build();

    let rstream = loop {
        if err_count > 10 {
            error!(
                "Abort: Too many failures during SSL handshake: {}",
                &host_no_port
            );
            return None;
        }
        match connector.connect(host_no_port, rstream_std) {
            Ok(n) => break n,
            Err(e) => {
                err_count += 1;
                match e {
                    HandshakeError::WouldBlock(eb) => {
                        error!("Another WouldBlock: {:?}", eb);
                        continue;
                    }
                    HandshakeError::SetupFailure(err) => {
                        error!(
                            "Abort: SetupFailure mid SSL handshake: {:?}",
                            err
                        );
                        return None;
                    }
                    _ => {
                        continue;
                    }
                }
            }
        }
    };

    Some(rstream)
}
