use clap::Parser;

/// MITM Proxy server that stores requests in sqlite for later usage
#[derive(Parser, Debug, Clone)]
pub struct Config {

    /// Path for the project to be stored
    #[clap(short, long)]
    directory: String,

    /// addr to bind to
    #[clap(short, long, default_value="127.0.0.1")]
    addr: String,

    /// port to bind to
    #[clap(short, long, default_value="8000")]
    port: usize
}

impl Config {
    pub fn new(directory: String, addr: String, port: usize) -> Self {
        Config {
            directory,
            addr,
            port
        }
    }

    pub fn directory(&self) -> &String {
        &self.directory
    }

    pub fn addr(&self) -> &String {
        &self.addr
    }

    pub fn port(&self) -> usize {
        self.port
    }

    pub fn full_addr(&self) -> String {
        format!("{}:{}", self.addr, self.port)
    }

}