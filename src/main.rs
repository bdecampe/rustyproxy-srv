use rusqlite::Connection;
use std::{error::Error, fs, path::Path};
use tokio::net::TcpListener;
use clap::Parser;
use log::{LevelFilter, info};
use env_logger::Builder;
use chrono::Local;
use std::io::Write;

mod client_manager;
mod ssl;
mod ssl_manager;
mod config;

use crate::client_manager::client_manager;
use crate::ssl_manager::get_ca_pair;
use crate::config::Config;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {

    Builder::new()
        .format(|buf, record| {
            writeln!(buf,
                "{} [{}] - {}",
                Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter(None, LevelFilter::Info)
        .init();

    let config = Config::parse();
    match Path::new(config.directory()).exists() {
        true => {}
        false => {
            fs::create_dir(config.directory())?;
        }
    }

    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS history(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        uri TEXT,
        method TEXT,
        params INTEGER,
        status INTEGER,
        size INTEGER,
        raw BLOB,
        ssl INTEGER,
        response TEXT,
        response_time TEXT)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS ca(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        hostname TEXT,
        cert TEXT,
        private TEXT)",
        [],
    )?;

    let addr = config.full_addr();
    let listener = TcpListener::bind(&addr).await?;

    info!("Listening on: {}", addr);

    let conn = Connection::open(format!("{}/hist.db", config.directory())).unwrap();

    let (ca_cert, ca_key_pair) = get_ca_pair(&conn);

    loop {
        let (stream, _peer_addr) = listener.accept().await?;
        let ca_cert = ca_cert.clone();
        let ca_key_pair = ca_key_pair.clone();
        let conf = config.clone();
        tokio::spawn(async move {
            client_manager(stream, ca_cert, ca_key_pair, conf).await;
        });

    }
}
