# rustyproxy-srv

This is server for [the rustyproxy project](https://gitlab.com/r2367/).

## Contributing

At this point the priority is making a user interface to be able to view/edit/replay requests. The overall goal is to provide the equivalent of the repeater and intruder of Burpsuite. But this part of the project should only store request coming from browsers. 

TL;DR: Message me on Matrix and i'll guide you: [@vaelio](https://matrix.to/#/@vaelio:matarch.fr)

## Getting started

If you are using the binary:

```sh
$ rustyproxy -h

rustyproxy
MITM Proxy server that stores requests in sqlite for later usage

USAGE:
    rustyproxy [OPTIONS] --directory <DIRECTORY>

OPTIONS:
    -a, --addr <ADDR>              addr to bind to [default: 127.0.0.1]
    -d, --directory <DIRECTORY>    Path for the project to be stored
    -h, --help                     Print help information
    -p, --port <PORT>              port to bind to [default: 8000]

```

if you are using cargo:

```sh
$ cargo run -- [OPTIONS] --directory <DIRECTORY>
```

For example:

```sh
$ cargo run --release -- -a 0.0.0.0 -p 8080 -d /tmp/rustyproxy-project/
```

## Roadmap

This part of the project is now in a somewhat usable state. 
There are still bugs that I need to trace but it should be mostly fine.
As a work around if you have any high CPU usage just restart the proxy and it should be fine.
If you ever have the time to trace which requests created the problem feel free to send it to me so that I can reproduce and fix.

- [x] HTTPS Mitm proxy
- [x] HTTP proxy
- [ ] Migrate from SQLite to something that could be used remotely by a client
- [ ] Upstream Proxy
- [ ] Code optimization and bug termination
